// ====================================================================================================================
// Simple movement of the camera around the scene
// Created by Leslie Young
// http://www.plyoung.com/ or http://plyoung.wordpress.com/
// ====================================================================================================================

using UnityEngine;

public class CameraMove : MonoBehaviour
{
	public float speed = 10f;
	public Transform target;			// target to follow (cam is fixed to following this around till it is NULL)
	public bool followTarget = false;	// follow the target? (only if target is not NULL)
	public bool doRotation = false;		// rotate to align with the target?
	public Transform camTr;
	public Vector2 min_xz;
	public Vector2 max_xz;
	private Transform tr;

	public delegate void CamMaunallyMoved();
	public CamMaunallyMoved OnCamManuallyMoved = null;
	
	void Awake()
	{
		useGUILayout = false;	// disable Unity GUI
	}
	
	void Start()
	{
		tr = this.transform;
		if (target && followTarget)
			tr.position = target.position;
	}

	void LateUpdate()
	{
		if (target && followTarget)
		{
			Vector3 difference = target.position - tr.position;
			tr.position = Vector3.Slerp(tr.position, target.position, Time.deltaTime * Mathf.Clamp(difference.magnitude, 0f, 2f));
			
			if( doRotation)
				tr.rotation = target.rotation;
		}
	}

	public void Follow(bool doFollowCurrentTarget)
	{
		followTarget = doFollowCurrentTarget;
	}

	public void Follow(Transform t)
	{
		target = t;
		followTarget = true;
	}

	public void SetRotation( Vector3 newRot)
	{
		Debug.Log ( "Setting Camera Rotation: " + newRot.y );
		tr.Rotate( newRot);
	}
	// ====================================================================================================================
}
