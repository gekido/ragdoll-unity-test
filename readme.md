# Ragdoll && Mecanim Unity Experiments

Experimenting with Mecanim & Unity 3d.

This repo is entirely based on Perttu Hämäläinen's experiments from the blog post at http://perttuh.blogspot.ca/2013/10/unity-mecanim-and-ragdolls.html
His site is http://perttu.info

All Credit goes to him for the awesome starting point.  Anything that sucks or is broken is probably my fault.

Mike W
http://www.gekidoslair.com
http://twitter.com/gekido

